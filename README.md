# road-crossing-distance

Calculate distance pedestrian must walk in order to cross the street (to reach nearest pedestrian crossing). Generates CSV files that can be later
used in QGis or similar software to generate images like this:

![Saint Petersburg map](doc/spb.png)

Each dot on this image is a point on a road or a street. Dot color represents, how much longer
will it be to use 'official' pavements and crossings to cross road at this point, rather than walking
over the road in a straight line.

* Green dots mean less than 100m extra walking
* Yellow - 100-200m (this is the limit for Russina road standards)
* Orange 200-400m
* Red 400-800m (which is really long)
* Dark red > 800m

Red points show places with poor pedestrian infrastructure, like this place:

![Bad pedestrian infrastructure example](doc/path.jpg)

Here pedestrians need to walk for extra 1.5km to just cross the road and get into the park from a tram stop.

## Requirements

Project uses Java 8 and Gradle 4
Source code utilizes Lombok java library https://projectlombok.org. In order for it to work you will need
to enable annotation processing, instructions for common IDEs are here https://immutables.github.io/apt.html.

## Running

### From IDE

Don't forget to enable annotation processing in your IDE!

Use ru.ifmo.idu.road_crossing.Main class. It takes 3 arguments:

1. Path to OSM XML file. Export one from https://openstreetmap.org using 'export' button at the top
2. City name, as used in OSM. Used to get city boundary
3. City shortcut name.

Output files will be stored in ./<city shortcut>_output/ directory

### From command-line

See parameter descriptions above

```
gradle build
gradle run --args="<path to OSM XML file> <City Name> <City shortcut>"
```

## File format

Result CSV file (output.csv) contains road point, each row is a single point. It has
following columns:

* lat road, lon road - point coordinates in WGS84
* lat first, lon first, lat second, lon second - coordinates of points on pavements around the road (one
to the left and one to the right)
* straight distance - straight line distance between first and second points
* path distance - length of route built by Graphopper library between first and second point
* ratio = path distance / straight distance
* difference = path distance - straight distance, this is how much longer pedestrian will need to walk if he wants to cross the road using
official crossing

## Computed data

If you just want existing results, go to [Downloads](https://bitbucket.org/itmo_idu/road-crossing-distance/downloads/) section for CSV and PNG downloads.