package ru.ifmo.idu.road_crossing;

import com.graphhopper.PathWrapper;
import lombok.Data;
import org.locationtech.jts.geom.Coordinate;

@Data
public class PointPair {

    RoadPoint roadPoint;

    Coordinate first;

    Coordinate second;

    double straightDistance;

    PathWrapper path;

    public PointPair(RoadPoint roadPoint, Coordinate first, Coordinate second, double distance) {
        this.roadPoint = roadPoint;
        this.first = first;
        this.second = second;
        this.straightDistance = distance;
    }
}
