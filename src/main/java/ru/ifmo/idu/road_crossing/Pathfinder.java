package ru.ifmo.idu.road_crossing;

import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.GraphHopper;
import com.graphhopper.reader.osm.GraphHopperOSM;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.storage.GraphHopperStorage;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

@Slf4j
public class Pathfinder {

    public static void findPaths(String osmFile, File graphHopperCacheDir, List<PointPair> points) {
        log.info("Setting up GraphHopper instance using input file {}", osmFile);

        GraphHopper hopper = new GraphHopperOSM().forDesktop();
        hopper.setDataReaderFile(osmFile);

        hopper.setCHEnabled(true);

        hopper.setGraphHopperLocation(graphHopperCacheDir.getAbsolutePath());
        hopper.setEncodingManager(EncodingManager.create("foot"));
        hopper.getCHFactoryDecorator().addWeighting("fastest");

        final long start = System.currentTimeMillis();
        hopper.importOrLoad();
        log.info("GraphHopper loaded in {} ms", System.currentTimeMillis() - start);

        log.info("Starting pathfinding...");
        int completed = 0;

        for (Iterator<PointPair> iterator = points.iterator(); iterator.hasNext(); ) {
            PointPair pp = iterator.next();
            if (++completed % 300 == 0) {
                log.info("Completed {} out of {}", completed, points.size());
            }
            GHRequest req = new GHRequest(pp.first.x, pp.first.y, pp.second.x, pp.second.y).
                    setWeighting("fastest").
                    setVehicle("foot").
                    setLocale(Locale.US);
            GHResponse rsp = hopper.route(req);

            if (rsp.hasErrors()) {
                log.warn("Failed to find path between {} and {}", pp.first, pp.second);
                iterator.remove();
                continue;
            }

            pp.path = rsp.getBest();
        }
        hopper.close();
    }
}
