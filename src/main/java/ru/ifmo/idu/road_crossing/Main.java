package ru.ifmo.idu.road_crossing;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Locale;

@Slf4j
public class Main {

    public static void dumpCsv(File file, List<PointPair> pointPairList) throws IOException {
        log.info("Saving to output...");
        try (PrintWriter pw = new PrintWriter(new FileWriter(file))) {
            pw.println("lat road, lon road, lat first, lon first, lat second, lon second, straight distance, path distance, ratio, difference");
            for (PointPair pp : pointPairList) {
                pw.printf(Locale.US, "%f, %f, %f, %f, %f, %f, %f, %f, %f, %f\n",
                        pp.roadPoint.point.x,
                        pp.roadPoint.point.y,
                        pp.first.x,
                        pp.first.y,
                        pp.second.x,
                        pp.second.y,
                        pp.straightDistance,
                        pp.path.getDistance(),
                        pp.path.getDistance() / pp.straightDistance,
                        pp.path.getDistance() - pp.straightDistance
                );
            }
        }
    }

    public static void main(String[] args) throws IOException {
        if (args.length < 3) {
            System.err.println("Usage: <osm file> <city name> <city key>");
            return;
        }
        File outputDir = new File("output_" + args[2]);
        log.info("Will save results to {}", outputDir.getAbsolutePath());
        log.info("Extracting points...");
        List<PointPair> pointPairList = PointsExtractor.extractPoints(args[1], args[0]);
        log.info("Doing pathfinding...");

        if (!outputDir.exists()) {
            outputDir.mkdirs();
        }
        Pathfinder.findPaths(args[0], new File(outputDir, "graphopper"), pointPairList);
        dumpCsv(new File(outputDir, "output.csv"), pointPairList);
        log.info("All done");
    }
}
