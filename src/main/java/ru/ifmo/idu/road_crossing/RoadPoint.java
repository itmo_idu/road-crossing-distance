package ru.ifmo.idu.road_crossing;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.math.Vector2D;

@Data
@AllArgsConstructor
public class RoadPoint {
    Coordinate point;

    double roadBearing;
}
