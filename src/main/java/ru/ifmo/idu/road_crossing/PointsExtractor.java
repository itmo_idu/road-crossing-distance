package ru.ifmo.idu.road_crossing;

import com.graphhopper.util.DistanceCalc;
import com.graphhopper.util.DistanceCalcEarth;
import com.graphhopper.util.shapes.GHPoint;
import de.topobyte.osm4j.core.access.OsmIterator;
import de.topobyte.osm4j.core.model.iface.*;
import de.topobyte.osm4j.core.model.util.OsmModelUtil;
import de.topobyte.osm4j.xml.dynsax.OsmXmlIterator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.locationtech.jts.geom.*;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;
import org.locationtech.jts.geom.prep.PreparedGeometry;
import org.locationtech.jts.geom.prep.PreparedGeometryFactory;
import org.locationtech.jts.index.quadtree.Quadtree;
import org.locationtech.jts.operation.linemerge.LineMerger;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class PointsExtractor {

    private static GeometryFactory geometryFactory = new GeometryFactory();

    private static final int meters = 20;

    private static final double minMeters = 1;

    // great value, but some wide boulevards have this distance between motorway and pedestrian footpath
    private static final double maxMeters = 150;

    private static DistanceCalc distanceCalc = new DistanceCalcEarth();


    private static class Context {
        Map<Long, Coordinate> nodeCache = new HashMap<>();

        Map<Long, CoordinateArraySequence> waysCache = new HashMap<>();

        List<LineString> motorways = new ArrayList<>(4096);

        List<LineString> pedestrianWays = new ArrayList<>(4096);

        Quadtree pedestrianQuadTree = new Quadtree();

        private Geometry cityBoundary;

        private PreparedGeometry preparedBoundary;
    }

    static CoordinateArraySequence getCoordinates(Context context, OsmWay way) {
        Coordinate[] coordinates = new Coordinate[way.getNumberOfNodes()];
        for (int idx = 0; idx < way.getNumberOfNodes(); ++idx) {
            coordinates[idx] = context.nodeCache.get(way.getNodeId(idx));
        }

        return new CoordinateArraySequence(coordinates);
    }

    private static boolean isClosed(OsmWay way) {
        return way.getNodeId(0) == way.getNodeId(way.getNumberOfNodes() - 1);
    }

    private static boolean isClosed(CoordinateArraySequence way) {
        return way.getCoordinate(0) == way.getCoordinate(way.size() - 1);
    }

    /**
     * Relation boundary can be given as a set of non-closed strings
     */
    private static Geometry createPolygonsFromLineSegments(List<CoordinateArraySequence> ways) {
        Geometry rz = null;
        LineMerger lineMerger = new LineMerger();
        for (CoordinateArraySequence way: ways) {
            lineMerger.add(new LineString(way, geometryFactory));
        }
        for (Object o: lineMerger.getMergedLineStrings()) {
            LineString ls = (LineString) o;
            if (!ls.isClosed()) {
                Coordinate[] closedCoords = new Coordinate[ls.getCoordinates().length + 1];
                System.arraycopy(ls.getCoordinates(), 0, closedCoords, 0, ls.getCoordinates().length);
                closedCoords[closedCoords.length - 1] = closedCoords[0];

                ls = new LineString(new CoordinateArraySequence(closedCoords), geometryFactory);
            }
            Polygon p = new Polygon(new LinearRing(ls.getCoordinateSequence(), geometryFactory), null, geometryFactory);
            if (rz == null) {
                rz = p;
            } else {
                rz = rz.union(p);
            }

        }
        return rz;
    }

    public static Geometry convertOSMGeometry(Context context, EntityContainer container) {
        Geometry baseGeometry;
        switch (container.getType()) {
            case Node:
                OsmNode node = (OsmNode) container.getEntity();
                baseGeometry = new Point(new CoordinateArraySequence(
                        new Coordinate[]{new Coordinate(node.getLatitude(), node.getLongitude())}
                ), geometryFactory);
                break;
            case Way:
                OsmWay way = (OsmWay) container.getEntity();
                if (isClosed(way)) {
                    // this is a closed set which most likely represents an area (there are some exceptions, but they are for types of polygons we do not need like highways)
                    baseGeometry = new Polygon(new LinearRing(getCoordinates(context, way), geometryFactory), null, geometryFactory);
                } else {
                    // just a non-closed polyline
                    baseGeometry = new LineString(
                            getCoordinates(context, way),
                            geometryFactory);
                }
                break;
            case Relation:
                OsmRelation relation = (OsmRelation) container.getEntity();
                Geometry outerGeometry = null;
                List<CoordinateArraySequence> nonClosedWays = new ArrayList<>();
                for (int memberId = 0; memberId < relation.getNumberOfMembers(); ++memberId) {
                    OsmRelationMember member = relation.getMember(memberId);
                    if (member.getType() == EntityType.Way && "outer".equals(member.getRole())) {
                        CoordinateArraySequence outerBound = context.waysCache.get(member.getId());
                        if (outerBound == null) {
                            log.error("Relation member {} {} {} not found in cache", member.getRole(), member.getType(), member.getId());
                            continue;
                        }
                        if (!isClosed(outerBound)) {
                            nonClosedWays.add(outerBound);
                            continue;
                        }

                        Geometry newOuterGeom = new Polygon(new LinearRing(outerBound, geometryFactory),
                                null,
                                geometryFactory);
                        if (outerGeometry == null) {
                            outerGeometry = newOuterGeom;
                        } else {
                            outerGeometry = outerGeometry.union(newOuterGeom);
                        }
                    }
                }

                if (!nonClosedWays.isEmpty()) {
                    Geometry newOuterGeom = createPolygonsFromLineSegments(nonClosedWays);
                    if (outerGeometry == null) {
                        outerGeometry = newOuterGeom;
                    } else {
                        outerGeometry = outerGeometry.union(newOuterGeom);
                    }
                }

                if (outerGeometry == null) {
                    throw new IllegalStateException("Such relations are not supported");
                }
                baseGeometry = outerGeometry;
                break;
            default:
                throw new IllegalStateException("Unsupported OSM entity type " + container.getType());
        }
        return baseGeometry;
    }


    private static void splitLongEdge(Context context,
                                      DistanceCalc distanceCalc,
                                      Coordinate start,
                                      Coordinate end,
                                      Set<RoadPoint> rz) {
        Coordinate candidate = new Coordinate((end.x + start.x) / 2, (end.y + start.y) / 2);
        rz.add(new RoadPoint(candidate, roadBearing(candidate, end)));

        if (distanceCalc.calcDist(start.x, start.y, candidate.x, candidate.y) > meters) {
            splitLongEdge(context, distanceCalc, start, candidate, rz);
        }

        if (distanceCalc.calcDist(candidate.x, candidate.y, end.x, end.y) > meters) {
            splitLongEdge(context, distanceCalc, candidate, end, rz);
        }
    }

    private static double roadBearing(Coordinate from, Coordinate to) {

        double dLon = (to.y - from.y);

        double y = Math.sin(dLon) * Math.cos(to.x);
        double x = Math.cos(from.x) * Math.sin(to.x) - Math.sin(from.x)
                * Math.cos(to.x) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;
        brng = 360 - brng; // count degrees counter-clockwise - remove to make clockwise

        return brng;
    }

    private static Set<RoadPoint> getNodes(Context context) {
        Set<RoadPoint> rz = new HashSet<>();
        long filtered = 0;
        for (LineString ls: context.motorways) {
            //todo: restore, gives strange boundary on small files
            /*if (!context.preparedBoundary.intersects(ls)) {
                continue;
            }*/
            Coordinate prevNode = null;
            for (Coordinate coord: ls.getCoordinates()) {
                if (prevNode != null) {
                    // ignore too close nodes
                    final double prevNodeDistance = distanceCalc.calcDist(prevNode.x, prevNode.y, coord.x, coord.y);
                    if (prevNodeDistance < meters / 2) {
                        ++filtered;
                        continue;
                    } else if (prevNodeDistance > meters) {
                        // add extra node in the middle
                        splitLongEdge(context, distanceCalc, prevNode, coord, rz);
                    }
                    final double roadBearing = roadBearing(prevNode, coord);
                    rz.add(new RoadPoint(coord, roadBearing));
                }

                prevNode = coord;

            }
        }

        log.info("Collected {} nodes, {} were filtered out", rz.size(), filtered);
        return rz;
    }

    private static Coordinate getPedestrianRoadNearby(Context context, RoadPoint rp, double angle) {
        GHPoint rightPoint = distanceCalc.projectCoordinate(
                rp.point.x,
                rp.point.y,
                maxMeters,
                angle
        );
        Geometry rightRay = new LineString(
                new CoordinateArraySequence(
                        new Coordinate[]{
                                rp.getPoint(),
                                new Coordinate(rightPoint.lat, rightPoint.lon)
                        }
                ), geometryFactory
        );

        Coordinate nearestIntersection = null;
        double nearestDistance = Double.MAX_VALUE;
        for (Object candidate: context.pedestrianQuadTree.query(rightRay.getEnvelopeInternal())) {
            LineString pedestrian = (LineString) candidate;
            Geometry intersection = pedestrian.intersection(rightRay);
            if (intersection.isEmpty()) {
                continue;
            }

            for (Coordinate intersectionPoint: intersection.getCoordinates()) {
                double intersectionDistance = distanceCalc.calcDist(rp.point.x, rp.point.y, intersectionPoint.x, intersectionPoint.y);
                if (intersectionDistance < minMeters) {
                    continue;
                }
                if (nearestIntersection == null || nearestDistance > intersectionDistance) {
                    nearestIntersection = intersectionPoint;
                    nearestDistance = intersectionDistance;
                }
            }
        }
        return nearestIntersection;

    }

    public static List<PointPair> getPointPairs(Context context, Collection<RoadPoint> roadPoints) {
        List<PointPair> result = Collections.synchronizedList(new ArrayList<>(roadPoints.size()));

        AtomicInteger count = new AtomicInteger();
        roadPoints.parallelStream().forEach(rp -> {
            if (count.incrementAndGet() % 500 == 0) {
                log.info("Processed {} out of {} ({}%)", count.get(), roadPoints.size(), count.get() / (float) roadPoints.size() * 100.0);
            }

            Coordinate rightCrossing = getPedestrianRoadNearby(context, rp, rp.roadBearing + 90);
            if (rightCrossing == null) {
                // no pedestrian footways nearby
                return;
            }
            Coordinate leftCrossing = getPedestrianRoadNearby(context, rp, rp.roadBearing - 90);
            if (leftCrossing == null) {
                return;
            }
            final double distance = distanceCalc.calcDist(rightCrossing.x, rightCrossing.y, leftCrossing.x, leftCrossing.y);
            if (distance < 1) {
                log.warn("Something is wrong, distance < 1m at point {}, {}", rp.point.x, rp.point.y);
                return;
            }
            result.add(new PointPair(
                    rp,
                    rightCrossing,
                    leftCrossing,
                    distance
            ));
        });
        return result;
    }

    private static Context readOSMFile(String cityName, String osmFile) throws IOException {
        Context context = new Context();
        Map<String, Geometry> streetMap = new HashMap<>();

        try (FileInputStream input = new FileInputStream(osmFile)) {
            OsmIterator iterator = new OsmXmlIterator(input, false);
            for (EntityContainer container: iterator) {
                try {
                    if (container.getType() == EntityType.Node) {
                        final OsmNode node = (OsmNode) container.getEntity();
                        context.nodeCache.put(container.getEntity().getId(), new Coordinate(node.getLatitude(), node.getLongitude()));
                    } else if (container.getType() == EntityType.Way) {
                        final OsmWay way = (OsmWay) container.getEntity();
                        context.waysCache.put(container.getEntity().getId(), getCoordinates(context, way));
                    }

                    Map<String, String> tags = OsmModelUtil.getTagsAsMap(container.getEntity());
                    if (StringUtils.equalsAny(tags.get("highway"),
                            "primary", "secondary", "tertiary", "unclassified", "residential")) {
                        Geometry geometry = convertOSMGeometry(context, container);
                        //todo: оставляет дырки в местах соединения участков дороги
//                        String name = tags.get("name");
//                        if (name != null) {
//                            Geometry buffer = bufferProjected(geometry, 30);
//                            Geometry existingBuffer = streetMap.get(name);
//                            if (existingBuffer == null) {
//                                streetMap.put(name, buffer);
//                            } else {
//                                geometry = geometry.difference(existingBuffer);
//                                if (geometry.isEmpty()) {
//                                    continue;
//                                } else {
//                                    streetMap.put(name, existingBuffer.union(buffer));
//                                }
//                            }
//
//                        }
                        if (!(geometry instanceof LineString)) {
                            log.warn("Road is not a line string");
                        } else {
                            context.motorways.add((LineString) geometry);
                        }
                    } else if (StringUtils.equalsAny(tags.get("highway"), "footway", "cycleway") || tags.containsKey("living_street")) {
                        if ("crossing".equals(tags.get("footway"))) {
                            continue;
                        }
                        final Geometry geometry = convertOSMGeometry(context, container);
                        if (!(geometry instanceof LineString)) {
                            log.warn("Road is not a line string");
                        } else {
                            final LineString pedestrianRoad = (LineString) geometry;
                            context.pedestrianWays.add(pedestrianRoad);
                            context.pedestrianQuadTree.insert(pedestrianRoad.getEnvelopeInternal(), pedestrianRoad);
                        }
                    } else if (container.getType() == EntityType.Relation && "city".equals(tags.get("place"))
                            && cityName.equalsIgnoreCase(tags.get("name"))) {
                        context.cityBoundary = convertOSMGeometry(context, container);
                        context.preparedBoundary = PreparedGeometryFactory.prepare(context.cityBoundary);
                    }


                } catch (Exception ex) {
                    log.warn("Error", ex);
                }
            }
        }

        log.info("{} motorways and {} pedestrian ways collected", context.motorways.size(), context.pedestrianWays.size());
        return context;
    }

    public static List<PointPair> extractPoints(String cityName, String osmFile) throws IOException {

        Context context = readOSMFile(cityName, osmFile);

        Set<RoadPoint> roadPoints = getNodes(context);

        log.info("Collected {} candidate points", roadPoints.size());

        List<PointPair> rz = getPointPairs(context, roadPoints);

        log.info("Collected {} point pairs", rz.size());

        return rz;
    }

    /**
     * Helper method to check a single road point
     *
     * @param cityName Name of city (used to detect city boundary)
     * @param osmFile  Path to OSM XML file
     * @param first    Coordinate of a point on a road
     * @param second   Coordinate of a next point on same road, to calculate road bearing
     * @return PointPair if this point is ok (has pedestrian footpaths nearby) or null otherwise
     * @throws IOException If failed to open osm file
     */
    public static List<PointPair> checkPoint(String cityName, String osmFile, Coordinate first, Coordinate second) throws IOException {
        log.info("Checking road at {}, {} ", first.x, first.y);
        Context context = readOSMFile(cityName, osmFile);
        List<RoadPoint> roadPoints = Collections.singletonList(
                new RoadPoint(first, roadBearing(first, second))
        );
        List<PointPair> rz = getPointPairs(context, roadPoints);
        if (rz.isEmpty()) {
            log.warn("This point is not valid");
        } else {
            log.info("This point is valid");
        }
        return rz;
    }

    private static Geometry bufferProjected(Geometry geom, double meters) {
        try {
            // extract the geometry
            MathTransform toTransform, fromTransform;
            geom = (Geometry) geom.clone();

            String code = "EPSG:3857";
            CoordinateReferenceSystem auto;
            auto = CRS.decode(code);
            toTransform = CRS.findMathTransform(
                    DefaultGeographicCRS.WGS84, auto);
            fromTransform = CRS.findMathTransform(auto,
                    DefaultGeographicCRS.WGS84);

            Geometry projectedGeom = JTS.transform(geom, toTransform);
            // buffer
            Geometry projectedBufferedGeom = projectedGeom.buffer(meters * 2, 4); // i have no idea why, but it builds 2 times smaller than needed
            // reproject the geometry to the original projection

            Geometry rz = JTS.transform(projectedBufferedGeom, fromTransform);
            return rz;
        } catch (Exception ex) {
            log.error("Failed to perform accurate buffer with projection", ex);
            return geom;
        }
    }

    public static void main(String[] args) throws IOException {
        if (args.length < 2) {
            System.err.println("Usage: <city name> <osm file>");
            return;
        }
        List<PointPair> pp = checkPoint(args[0],
                args[1],
                new Coordinate(59.8461, 30.1838),
                new Coordinate(59.8461, 30.1856)
        );
        Main.dumpCsv(new File("point_check.csv"), pp);
        log.info("All done");
    }
}
